import argparse
import subprocess
import os
import colorama
from enum import Enum

# Кодировка для чтения вывода запускаемых процессов
cmd_encoding = 'cp866'

# Собственно, тот кто занимается построением и обеспечивает нам вывод информации в консольку
# (используется VS2019)
builder = "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\Common7\\IDE\\devenv.com"

# Директория для всех проектов
projects_dir = "D:\\GeoS\\K3-SVN\\TRUNK\\"

# Каждый из проектов пока будет здесь жестко закодирован
domus_sln = "Domus\\PRJ\\Domus.sln"
dom_sln = "Dom\\PRJ\\Dom.sln"
k325_sln = "K325\\PRJ\\K325.sln"
mebel_sln = "Mebel\\PRJ\\MEBEL.sln"
tent_sln = "Tent\\Prj\\Tent.sln"

# Список решений на построение
solutions = [ domus_sln, dom_sln, k325_sln, mebel_sln, tent_sln ]


# Типы сообщений вывода
class OutputMessageType(Enum):
    REGULAR = 0
    WARNING = 1
    ERROR = 2
    FILENAME = 3
    LAST_LINE = 4


# Сообщения об ошибках
class ErrorMsg:
    @staticmethod
    def bad_path_msg(path):
        print('Путь не существует: "' + path + '"!')


def is_error(string):
    return string.lower().find(' error') != -1 and string.lower().find('no error') == -1


def is_warning(string):
    return string.lower().find('warning') != -1


def is_file_name(string):
    return string.lower().find('.cpp') != -1


def is_last_line(string):
    return string.lower().find('=') != -1


# Узнать, что за сообщение было выдано
def get_message_type(message):
    if is_warning(message):
        return OutputMessageType.WARNING
    elif is_error(message):
        return OutputMessageType.ERROR
    elif is_file_name(message):
        return OutputMessageType.FILENAME
    elif is_last_line(message):
        return OutputMessageType.LAST_LINE
    else:
        return OutputMessageType.REGULAR


# Печатаем строку (с расскраской)
def print_line(msg_str, msg_type):
    color = colorama.Fore.RESET
    if msg_type == OutputMessageType.WARNING:
        color = colorama.Fore.LIGHTYELLOW_EX
    elif msg_type == OutputMessageType.ERROR:
        color = colorama.Fore.LIGHTRED_EX
    print(color + msg_str)


# Разбираемся с выводом от процесса-построителя
def print_build_output(proc):
    for line in proc.stdout:
        # Анализируем строку
        msg = line.decode(cmd_encoding, errors='replace').replace('\n', '')
        t = get_message_type(msg)

        # Не будем выводить никакие сообщения
        if t != OutputMessageType.ERROR and t != OutputMessageType.WARNING and t != OutputMessageType.LAST_LINE:
            continue

        if t == OutputMessageType.LAST_LINE:
            msg += '\n'

        # вывести окрашенную строку
        print_line(msg, t)

    for line in proc.stderr:
        msg = line.decode(cmd_encoding, errors='replace').replace('\n', '')
        print_line(msg, OutputMessageType.ERROR)


def get_sln_name(path):
    return os.path.basename(path)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--rebuild', default=False, action='store_true', help='make forсe rebuild')
    parser.add_argument('-cf', '--config', default='UniDebug-x64',
                        help='<configuration_name>|<platform_name>')
    args = parser.parse_args()

    action = ['/build', '/rebuild'][args.rebuild]

    config = args.config.replace('-', '|')

    proc = None
    try:
        for sol in solutions:
            path = os.path.normpath(projects_dir + sol)
            if not os.path.exists(path):
                ErrorMsg.bad_path_msg(path)
                continue

            print('Строим решение: ' + get_sln_name(sol) + ' ' + config)

            cmd = [builder, path, action, config]

            # Запускаем построение (дочерние процессы)
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            # Вывод информации, выдаваемой при построении, в консоль
            print_build_output(proc)

    # Был произведен досрочный выход по Ctrl+C
    # Убиваем процессы построения, если они были запущены
    except KeyboardInterrupt:
        print('\nCanceled!')
        if proc is not None:
            proc_killer = subprocess.Popen(['taskkill', '/F', '/T', '/PID', str(proc.pid)],
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)
            for line in proc_killer.stdout:
                print(line.decode(cmd_encoding, errors='replace').replace('\n', ''))
            for line in proc_killer.stderr:
                print(line.decode(cmd_encoding, errors='replace').replace('\n', ''))


if __name__ == '__main__':
    main()
